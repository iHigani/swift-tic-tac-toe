import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var eight: UIButton!

    var buttons : Array<UIButton> = []
    // for storing all UIButtons

    @IBOutlet weak var turnLabel: UILabel!
    // Turn label and Win status
    
    
    var turn = "X"
    var win = false
    
    var gameState = [ -1, -1, -1, -1, -1, -1, -1, -1 ]
    let winningStates = [ [0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6], [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6] ];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttons = [ zero, one, two, three, four, five, six, seven, eight ]
        initialize()
    }
    
    func initialize() {
        gameState = [ -1, -1, -1, -1, -1, -1, -1, -1, -1 ]
        for button in buttons {
            button.setTitle("", for: .normal)
        }
        win = false
        turn = "X"
        turnLabel.text = "X"
    }

    @IBAction func move(_ sender: UIButton) {
        if(gameState[sender.tag] != -1 || win) {
            return
        }

        sender.setTitle(turn, for: .normal)
        sender.setTitleColor(turn == "X" ? .red : .green, for: .normal)
        turnLabel.text = turn == "X" ? "O" : "X";
        gameState[sender.tag] = turn == "X" ? 1 : 0
        
        
        for state in winningStates {
            if(gameState[state[0]] == gameState[state[1]] && gameState[state[1]] == gameState[state[2]] && gameState[state[0]] != -1 ) {
                turnLabel.text = "Player " + turn + " Won !!"
                win = true
            }
        }
        
        let tie = gameState.firstIndex(of: -1);
        if (tie == nil) {
            turnLabel.text = "Tie !!"
        }
        
        
        turn = turn == "X" ? "O" : "X"
    }
    
    @IBAction func reset(_ sender: Any) {
        initialize()
    }
}
